<?php

declare(strict_types=1);

namespace Dexodus\Jsel;

use V8Js;

class Jsel
{
    private readonly JselContext $context;

    private readonly V8Js $v8Js;

    public function __construct(?JselContext $context = null)
    {
        $this->context = $context ?? new JselContext();
        $this->v8Js = new V8Js();
        $this->v8Js->context = $this->context->data;
        $this->v8Js->changeDataInContext = $this->changeDataInContext(...);

        $this->v8Js->setModuleLoader($this->moduleLoad(...));
        $this->v8Js->setModuleNormaliser($this->moduleNormalize(...));

        $result = $this->v8Js->executeString(<<<'JS'
    const {Jsel, JselContext} = require('jsel/bundle')
    const jselContext = new JselContext(PHP.context)
    const jsel = new Jsel(jselContext)
JS
        );
    }
    public function exec(string $code): mixed
    {
        $result = $this->v8Js->executeString("jsel.exec('$code')");
        $this->v8Js->executeString('PHP.changeDataInContext(jselContext.scope)');
        return $result;
    }

    private function moduleNormalize(string $base, $moduleName): array
    {
        if ($base === '') {
            $base = __DIR__ . '/js';
        }

        $filepath = realpath($base . '/' . $moduleName . '.js');

        $path = str_replace('./', '', $filepath);
        $parts = explode('/', $path);

        return [
            implode('/', array_slice($parts, 0, count($parts) - 1)),
            $parts[count($parts) - 1],
        ];
    }

    private function moduleLoad(string $path): string
    {
        return file_get_contents($path);
    }

    private function changeDataInContext($data): void
    {
        $this->context->data = json_decode(json_encode($data), true);
    }
}
