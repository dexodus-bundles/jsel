<?php

declare(strict_types=1);

namespace Dexodus\Jsel;

class JselContext
{
    public function __construct(
        public array $data = [],
    ) {
    }
}
